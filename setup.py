import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'zope.sqlalchemy',
    'waitress',
    'pymongo',
    'pyyaml',
    'gitpython',
    'pylint',
    'celery',
    'pairtree',
    'webtest'
    ]

setup(name='lint-io',
      version='0.1',
      description='lint-io',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='Olivier Sallou',
      author_email='olivier.sallou@irisa.fr',
      url='',
      keywords='web pylint',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='lintio',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = lintio:main
      [console_scripts]
      initialize_lint-io_db = lintio.scripts.initializedb:main
      """,
      )
