from pairtree import *
import pairtree


class Repo:
    """Pairtree structure containing user repositories"""

    __settings = None
    __repo = None

    def __init__(self):
        pairtree = PairtreeStorageFactory()
        self.pairtree = pairtree.get_store(store_dir=Repo.__settings['repo']+'/lintio',
                                           uri_base="http://")

    @classmethod
    def settings(klass, args=None):
        if args is None:
            return Repo.__settings
        Repo.__settings = args
        return Repo.__settings

    @classmethod
    def get(klass):
        if Repo.__repo is None:
          Repo.__repo = Repo()
        return Repo.__repo

    def get_path(self, user):
        # Check it exists, else create it
        user_repo = Repo.__repo.pairtree.get_object(user)
        return os.path.join(Repo.__settings['repo'],'lintio','pairtree_root',
                            pairtree.id2path(user))

