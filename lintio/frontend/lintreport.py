#!/usr/bin/python

modules = ['mako', 'redis']

import sys
import logging
logging.basicConfig(level=logging.INFO)

import imp
for module in modules:
    try:
        imp.find_module(module)
    except ImportError:
        sys.stderr.write("Missing module "+module+" !!")
        sys.stderr.write("You need to install the following modules: mako, redis")
        sys.exit(1)

import os

import argparse
import redis
import json

from pymongo import MongoClient
from git import Repo as GitRepo

from lintio.repo import Repo


class Connection:

  __conn = None

  @classmethod
  def get(klass, args=None):
    if Connection.__conn is None:
        if args is None:
            Connection.__conn = MongoClient('localhost',27017)
        else:
            Connection.__conn = MongoClient(args['db_host'],
                                            int(args['db_port']))
    return Connection.__conn['lintio']

def extract(args):
    """
    Extract lines from code around lint line.
    :params args: Context parameters
    args['project']
    args['owner']
    args['branch']
    args['commit']
    args['repo']
    args['db_host']
    args['db_port']
    args['file']
    args['line']
    :type args: dict
    :return: dict
    """

    Repo.settings(args)
    project_dir = os.path.join(Repo.get().get_path(args['owner']),
                'code',
                args['project'],
                args['branch'],
                args['commit'])

    content = []
    lintline = int(args['line'])
    with open(os.path.join(project_dir,args['file'])) as f:
        content = f.readlines()

    before = ''
    minlimit  = max(lintline-10,0)
    for i in range(minlimit,lintline):
        before += content[i]

    after = ''
    maxlimit = min(lintline+5,len(content))
    for i in range(lintline,maxlimit):
        after += content[i]

    res = { "line" : lintline, "before": before.rstrip(), "after": after, "minlimit": minlimit, "maxlimit": maxlimit }
    return res



"""
parser = argparse.ArgumentParser(description='Clone repository and apply lint rules.')
parser.add_argument('--owner', dest="owner", help="repository owner")
parser.add_argument('--project', dest="project", help="Project name")
parser.add_argument('--commit', dest="commit", help="commit id")
parser.add_argument('--redis', dest="redis_host", help="Redis hostname, defaults localhost", default='localhost')

args = parser.parse_args()

if not args.project:
    sys.stderr.write('project is missing')
    sys.exit(1)
"""
def report(args):

    project = args['owner']+"_"+args['project']

    project_dir = os.path.join('repos',project)

    db = Connection.get(args)

    record = db.lintio.find_one({"owner": args["owner"],
                                 "project": args["project"]})

    if record is None:
        return { 'error': 'no project found'}

    lints = dict()
    for branch in record['lint']:
        logging.info("look at branch "+branch)
        if branch == args["branch"]:
            lints = record['lint'][branch]
            break


    mylint = None
    last = None
    for commit in lints:
        if last is None:
            last = int(commit)
            mylint = lints[commit]
        else:
            if int(commit) > last:
                last = int(commit)
                mylint = lints[commit]
    
    return json.loads(mylint)

"""
    from mako.template import Template
    from mako.lookup import TemplateLookup

    mylookup = TemplateLookup(directories=['.'])
    mytemplate = Template(filename='templates/index.html', lookup=mylookup)
    myfile = mytemplate.render(project=args['project'],
                            owner=args['owner'],
                            commit=mylint['info']['last_commit'],
                            commit_id=mylint['info']['commit_id'],
                            modules=sorted(modules),
                            lints=mylint, messages=json.dumps(lints))

    project_dir = os.path.join('../public/repos',
                            project,args['branch'],args['commit'])
    if not os.path.exists(project_dir):
        os.makedirs(project_dir)
    fo = open(os.path.join(project_dir,'index.html'), "w")
    fo.write(myfile)
    fo.close()
"""
