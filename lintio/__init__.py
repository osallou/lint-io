from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )

from lintio.repo import Repo


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Repo.settings(settings)
    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('analyse', '/{user]/lint')
    config.add_route('user_dashboard', '/{user}')
    config.add_route('project_dashboard', '/{user}/{project}')
    config.add_route('project_lint', '/{user}/{project}/{branch}')
    config.add_route('file_extract',
           '/{user}/{project}/{branch}/{commit}/file_extract')
    config.scan()
    return config.make_wsgi_app()
