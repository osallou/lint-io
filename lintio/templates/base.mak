<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="/static/css/normalize.css" />
<link rel="stylesheet" href="/static/css/foundation.min.css" />
<link rel="stylesheet" href="/static/css/codemirror.css">
<link rel="stylesheet" href="/static/css/lintio.css">
<script src="/static/js/codemirror-compressed.js"></script>
<title>lint-io</title>
</head>
<body>
<!-- Header and Nav -->
  
  <div class="row">
    <div class="large-4 columns">
      <h1><img src="http://placehold.it/400x100&text=Logo" /></h1>
    </div>
    <div class="large-8 columns">
      <ul class="inline-list right">
		<li>Login</li>
      </ul>
    </div>
  </div>
  
  <!-- End Header and Nav -->
  
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small
devices) but to be to the right of the nav on larger screens -->
    <div class="large-8 push-4 columns">
        ${self.body()} 
    </div>
    
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-4 pull-8 columns">
        
      <ul class="side-nav">
        <%block name="sidebar">

        </%block>

      </ul>
      
      <p><img src="http://placehold.it/320x240&text=Ad" /></p>
        
    </div>
    
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr />
      <div class="row">
        <div class="large-6 columns">
          <p>&copy; Olivier Sallou - IRISA, 2013.</p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>

  <script src="/static/js/foundation/foundation.js"></script>
  <script src="/static/js/foundation/foundation.alerts.js"></script>
  <script src="/static/js/foundation/foundation.clearing.js"></script>
  <script src="/static/js/foundation/foundation.cookie.js"></script>
  <script src="/static/js/foundation/foundation.dropdown.js"></script>
  <script src="/static/js/foundation/foundation.forms.js"></script>
  <script src="/static/js/foundation/foundation.joyride.js"></script>
  <script src="/static/js/foundation/foundation.magellan.js"></script>
  <script src="/static/js/foundation/foundation.orbit.js"></script>
  <script src="/static/js/foundation/foundation.placeholder.js"></script>
  <script src="/static/js/foundation/foundation.reveal.js"></script>
  <script src="/static/js/foundation/foundation.section.js"></script>
  <script src="/static/js/foundation/foundation.tooltips.js"></script>
  <script src="/static/js/foundation/foundation.topbar.js"></script>
  <script src="/static/js/foundation/foundation.interchange.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
