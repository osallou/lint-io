<%inherit file="base.mak"/>
<script src="/static/js/vendor/jquery.js"></script>

<h1>${project}</h1>
<ul>
% for branch in branches:
    <li><a href="/${user}/${project}/${branch}">${branch}</a></li>
% endfor
</ul>

