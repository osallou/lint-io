<%inherit file="base.mak"/>

<%!
    import json

    def to_json( d ):
        return json.dumps(d)
%>

<%block name="sidebar">
% for module in modules:
<div class="section-container accordion" data-section="accordion">
  <section>
    <p class="title" data-section-title><a href="#">${module}</a></p>
    <div class="content" data-section-content>
      <p>
           <ul>
        % for file in lints['per'][module]:
            <li><a href="#" class="module" data-module="${module}"
data-file="${file}">${file} (${ len(lints['per'][module][file])})</a></li>
        % endfor
           </ul>
      </p>
    </div>
  </section>
</div>
% endfor
</%block>

<h1>${project} - ${branch}</h1>
<div id="statsrow" class="row">
<a class="button dropdown large expand" href="#" data-dropdown="stats">Statistics</a>
<ul id="stats" class="f-dropdown content" data-dropdown-content>
</ul>
</div>
<div class="row">
<div id="messages" class="section-container accordion" data-section="accordion">
</div>
</div>
<script>
function addEditor(elt, data) {
  var line = elt.attr('data-line');
  var beforecode = $("#beforecode"+line);
  if(beforecode.attr("data-loaded")=="1") {
    console.log("already loaded, skip loading");
	return;
  }
  var lid = elt.attr('data-id');
  var module = elt.attr('data-module');
  var file = elt.attr('data-file');

  var aftercode = $("#aftercode"+line);
  var beditor = CodeMirror.fromTextArea(beforecode.get(0), {
    mode: { name: "python", version: 2, singleLineStringErrors: false},
    lineNumbers: true,
    indentUnit: 4,
    tabMode: "shift",
    matchBrackets: true,
	readOnly: true,
    viewportMargin: Infinity,
    firstLineNumber: data['minlimit']+1
  });
  var aeditor = CodeMirror.fromTextArea(aftercode.get(0), {
    mode: { name: "python", version: 2, singleLineStringErrors: false},
    lineNumbers: true,
    indentUnit: 4,
    tabMode: "shift",
    matchBrackets: true,
    readOnly: true,
    viewportMargin: Infinity,
    firstLineNumber: parseInt(line)+1
  });
  beditor.setValue(data['before']);
  aeditor.setValue(data['after']);
  $('#messagecode'+line).html('<div class="lint-info">'+lintio['per'][module][file][lid]['info']+'</div><code>'+lintio['per'][module][file][lid]['msg'].replace(/(\r\n|\n|\r)/g,"<br/>").replace(/\s/g,"&nbsp;")+'</code>');
  beforecode.attr("data-loaded","1");
}

</script>

<script src="/static/js/vendor/jquery.js"></script>
<script>
    var commit = "${commit}";
    var lintio = ${ to_json(lints) |n};
    $(document).ready(function() {

      var stats = "<table><thead><th>Code</th><th>Count</th></thead><tbody>";
      // Look at stats for last commit
      $.each(lintio['stats'], function(key,val) {
        stats += '<tr><td>'+key+'</td><td>'+val+'</td></tr>';

      });
      stats += '</tbody></table>';
	  $("#stats").html(stats);

      $(document).on('click','.lint', function() {
        elt = $(this);
		$.getJSON('/${owner}/${project}/${branch}/${commit_id}/file_extract',
        { file: $(this).attr('data-file'),
          line: $(this).attr('data-line')
        },
        function(data) {
            addEditor(elt,data);
		});

      });

      $('.module').click(function() {
        $("#messages").html('');

        var file = $(this).attr('data-file');
        var module = $(this).attr('data-module'); 
        lintio['per'][module][file].sort(sortByLine);
        for(var i=0;i<lintio['per'][module][file].length;i++) {
          var line = lintio['per'][module][file][i]['line'];
          // file, line, info
          var msg = '<section data-section-title><p class="title" data-section-title>';
		  msg += '<a href="#" class="lint" data-module="'+module+'" data-file="'+file+'" data-line="'+line+'" data-id="'+i+'">'+line+":"+lintio['per'][module][file][i]['info']+'</a></p>';
		  msg += '<div class="content" data-section-content>';
		  msg += '<div><textarea data-loaded="0" id="beforecode'+line+'" class="codearea"></textarea><div class="messagecode" id="messagecode'+line+'"></div><textarea class="codearea" id="aftercode'+line+'"></textarea></div>';
          msg += '</div></section>';
		$("#messages").append(msg);
        }
      });
    });
    function sortByLine(ia,ib) {
      var a = parseInt(ia['line']);
      var b = parseInt(ib['line']);
      return (a - b);
    }

</script>
