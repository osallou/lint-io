<%inherit file="base.mak"/>
<script src="/static/js/vendor/jquery.js"></script>

<h1>Projects</h1>
<ul>
% for project in projects:
    <li><a href="/${user}/${project}">${project}</a></li>
% endfor
</ul>
