from pyramid.response import Response
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    )

import lintio.backend
from lintio.backend.lint  import lint
from lintio.frontend.lintreport import report, extract
import json

import pymongo

@view_config(route_name='user_dashboard', renderer='lintio:templates/dashboard.mak')
def user_dashboard(request):
    args = {}
    args['db_host'] = request.registry.settings['mongo_server']
    args['db_port'] = int(request.registry.settings['mongo_port'])
    conn  = pymongo.Connection(args['db_host'],args['db_port'])
    db = conn.lintio
    projects = []
    db_projects = db.lintio.find({"owner": request.matchdict['user']},
                                 { 'project': 1 })
    for project in db_projects:
        projects.append(project['project'])

    return { 'user': request.matchdict['user'], 'projects' : projects}

@view_config(route_name='project_dashboard', renderer='lintio:templates/project_dashboard.mak')
def project_dashboard(request):
    args = {}
    args['db_host'] = request.registry.settings['mongo_server']
    args['db_port'] = int(request.registry.settings['mongo_port'])
    conn  = pymongo.Connection(args['db_host'],args['db_port'])
    db = conn.lintio
    branches = []
    db_branches = db.lintio.find_one({"owner": request.matchdict['user'],
                                  "project": request.matchdict['project']})
    for branch in db_branches['lint']:
        branches.append(branch)
    return { 'branches': branches, 'user': request.matchdict['user'],
             'project':  request.matchdict['project']}

@view_config(route_name='file_extract', renderer='json')
def file_extract(request):
    # '/${owner}/${project}/${branch}/${commit}/file_extract')a
    args = {}
    args['project'] = request.matchdict['project']
    args['owner'] = request.matchdict['user']
    args['branch'] = request.matchdict['branch']
    args['commit'] = request.matchdict['commit']
    args['repo'] = request.registry.settings['repo']
    args['db_host'] = request.registry.settings['mongo_server']
    args['db_port'] = request.registry.settings['mongo_port']
    args['file'] = request.params.getone('file')
    args['line'] = request.params.getone('line')
    return extract(args)


@view_config(route_name='project_lint', renderer='lintio:templates/project.mak')
def project_lint(request):
    args = {}
    args['project'] = request.matchdict['project']
    args['owner'] = request.matchdict['user']
    args['branch'] = request.matchdict['branch']

    args['db_host'] = request.registry.settings['mongo_server']
    args['db_port'] = request.registry.settings['mongo_port']
    args['repo'] = request.registry.settings['repo']

    # TO fix pb with skitsh.js
    if args['project'] == 'modules':
        return HTTPNotFound('There is no such resource')
        


    mylint = report(args)
    modules = []

    for module in mylint["modules"]:
        modules.append(module)

    return { "lints" : mylint, "modules": sorted(modules),
             "project": args['project'], "owner": args['owner'],
             "commit": mylint['info']['last_commit'],
             "commit_id": mylint['info']['commit_id'],
             "branch": args['branch']}

@view_config(route_name='analyse', renderer='json')
def analyse(request):
    args = {}
    args['lint'] = None
    args['owner'] = request.matchdict['user']
    try:
        # Github
        params = request.params.getone('payload')
        params = json.loads(params)
        args['project'] = params['repository']['name']
        #args['email'] = params['repository']['owner']['email']
        args['url'] = params['repository']['url']
        args['commit'] = params['head_commit']['id']
        args['branch'] = params['ref']
    except:
        # URL params
        params = request.params
        args['project'] = params.getone('project')
        #args['email'] = params.getone('owner')
        args['url'] = params.getone('url')
        args['commit'] = params.getone('commit')
        args['branch'] = params.getone('branch')

    args['db_host'] = request.registry.settings['mongo_server']
    args['db_port'] = request.registry.settings['mongo_port']
    args['repo'] = request.registry.settings['repo']
    if 'lint' in request.registry.settings:
        args['lint'] = request.registry.settings['lint']
    if request.registry.settings['delay'] == 1:
        lint(args).delay()
    else:
        lint(args)
    return {'status' : 0}



@view_config(route_name='home', renderer='lintio:templates/index.mak')
def my_view(request):
    return {}

conn_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_lint-io_db" script
    to initialize your database tables.  Check your virtual 
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""

