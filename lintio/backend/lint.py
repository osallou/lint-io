#!/usr/bin/python

modules = ['yaml', 'git', 'pylint', 'redis']

import sys
import shutil
import logging
logging.basicConfig(level=logging.INFO)

import imp
for module in modules:
    try:
        imp.find_module(module)
    except ImportError:
        sys.stderr.write("Missing module "+module+" !!")
        sys.stderr.write("You need to install the following modules: pyyaml, gitpython, pylint, redis")
        sys.exit(1)

import yaml
import os
from git import Repo as GitRepo

import argparse
import fileinput
import re
import redis
import json

def fill_db(pyfile, lints, info={}):
    commit = lints[info["branch"]][info['last_commit']]
    lint = None
    msg = ""
    module = pyfile.replace('pylint_','').replace('.txt','')
    if module not in commit['modules']:
        commit['modules'][module] = []

    last = ''
    for line in fileinput.input([pyfile]):
        if re.match('^.*:\d+:',line):
            if lint is not None:
                # Fill in db
                lint['msg'] = msg
                 # If equal, this is duplicate error, skip it
                if lint['file']+'_'+lint['line'] != last:
                    commit['modules'][module].append(lint)
                    last = lint['file']+'_'+lint['line']
                lint = None
            msg = ""
            lint_warning = line.split(':')
            code = re.findall('^\s+\[(.*\(.*\)),',lint_warning[2])[0]
            if code not in commit['stats']:
                commit['stats'][code] = 1
            else:
                commit['stats'][code] += 1
            lint = {'file': lint_warning[0], 'line': lint_warning[1], 'info': lint_warning[2]}
        else:
            msg += line

    modules = []
    per = {}
    for module in commit['modules']:
        modules.append(module)
        per[module] = {}
        for filemodule in commit['modules'][module]:
            mfile = filemodule['file']
            if mfile not in per[module]:
                per[module][mfile] = []
            per[module][mfile].append(filemodule)
    commit['per'] = per

    return lints


"""
parser = argparse.ArgumentParser(description='Clone repository and apply lint rules.')
parser.add_argument('--owner', dest="owner", help="repository owner")
parser.add_argument('--project', dest="project", help="Project name")
parser.add_argument('--url', dest="repo", help="Git repository url")
parser.add_argument('--lint', dest="lint",
        help="Path to lint file, defaults to project repo/.lint-io")
parser.add_argument('--redis', dest="redis_host",
        help="Redis hostname, defaults localhost", default='localhost')
parser.add_argument('--commit', dest="commit",
        help="Commit hash id")
parser.add_argument('--branch', dest="branch",
        help="git branch, defaults to master", default="master")
parser.add_argument('--max', dest="max",
        help="max number of commit history", default=10)

args = parser.parse_args()
"""

from celery import task
from lintio.repo import Repo

@task
def lint(args):
    Repo.settings(args)
    project_dir = os.path.join(Repo.get().get_path(args['owner']),
                'code',
                args['project'],
                args['branch'],
                args['commit'])
    project_repo = args['url']
    project_lint =  os.path.join(project_dir,'.lint-io')
    if args['lint'] is not None:
        project_lint = args['lint']

    repo = None
    if not os.path.exists(project_dir):
        logging.info("Clone repository")
        repo = GitRepo.clone_from(project_repo, project_dir)
    else:
        logging.info("Update repository")
        repo = GitRepo(project_dir)
        origin = repo.remotes.origin
        origin.pull()

    git = repo.git
    if args['branch'] is not "master":
        git.checkout('origin/'+args['branch'], b=args['branch'])
    # Check if specific commit, else keep head
    if 'commit' in args:
        git.checkout(args['commit'],'.')
    else:
        args['commit'] = repo.heads.master.commit.hexsha

    info = {}
    info["last_commit"] = repo.heads.master.commit.committed_date
    info["commit_id"] = args['commit']
    info["project"] = args['owner']+'_'+args['project']
    info["branch"] = args['branch']
    info["owner"] = args['owner']
    if 'commiter' in args:
        info['commiter'] = args['commiter']

    fconfig = open(project_lint)
    configdata = yaml.load(fconfig)
    fconfig.close()

    # If we limit to a set of branches, check current one, else skip
    if 'branch' in configdata:
        if re.match(configdata['branch'], info['branch']) is None:
            return

    modules = configdata['modules']

    logging.info("Using modules "+str(modules))

    # Execute pylint
    from pylint import lint

    lintargs = ['--reports=n', '--msg-template={path}:{line}: [{msg_id}({symbol}), {obj}] {msg}', '--files-output=y']
    curdir = os.getcwd()
    os.chdir(project_dir)
    for module in modules:
        lintargsmodule = lintargs[:]
        lintargsmodule.append(module)
        lint.Run(lintargsmodule, exit=False)

    #from pymongo import MongoClient
    import pymongo
    from bson.objectid import ObjectId
    #conn = MongoClient(args['db_host'],args['db_port'])
    conn  = pymongo.Connection(args['db_host'],args['db_port'])
    db = conn.lintio
    record = db.lintio.find_one({"owner": args["owner"], "project": args["project"]})

    if record is None:
        record = {"owner": args["owner"], "project": args["project"], "lint": {}}
        record["_id"] = db.lintio.save(record)
    
    lints = record['lint']

    #r = redis.StrictRedis(host=args['db_host'], port=6379, db=0)
    #jsonlints = r.get(info['project'])
    #lints = dict()
    #if jsonlints is not None:
    #    lints = json.loads(jsonlints)


    if 'max' not in args:
        args['max'] = 1

    for branch_name in lints:
        branch_lint = lints[branch_name]
        # Clear previous commits
        commits = branch_lint.keys()
        print str(commits)
        commits.sort()
        last = len(commits) -1
        if info['last_commit'] == commits[last]:
            logging.info("analysing an existing commit, let's override it")

        if int(info['last_commit']) < int(commits[last]):
            logging.info("request to analyse an older commit, skipping it")
            return

        # We analyse a newer or same commit, remove old ones

        if len(commits)>=args['max']:
            toomuch = len(commits) - args['max']
            for i in range(0,toomuch):
                old = branch_lint[commits[i]]
                # Delete files/dir
                old_project_dir = \
                    os.path.join(Repo.get().get_path(old['info']['owner']),
                    'code',
                    old['info']['project'],
                    old['info']['branch'],
                    old['info']['commit_id'])
                old_public_dir = \
                    os.path.join(Repo.get().get_path(old['info']['owner']),
                    'public',
                    old['info']['project'],
                    old['info']['branch'],
                    old['info']['commit_id'])
                shutil.rmtree(old_project_dir)
                shutil.rmtree(old_public_dir)
                del lints[branch_name][commits[i]]
    
    # Reset current commit
    if info["branch"] not in lints:
        lints[info["branch"]] = dict()
    lints[info["branch"]][info['last_commit']] = dict()
    lints[info["branch"]][info['last_commit']]['info'] = info
    lints[info["branch"]][info['last_commit']]['modules'] = dict()
    lints[info["branch"]][info['last_commit']]['stats'] = dict()


    for pyfile in os.listdir("."):
        if pyfile.startswith("pylint_"):
            lints = fill_db(pyfile, lints, info)

    #r.set(info['project'], json.dumps(lints))
    #db.lintio.save(record)
    try:
        from bson import BSON
        elt = json.dumps(lints[info["branch"]][info['last_commit']])
        #elt = lints[info["branch"]][info['last_commit']]
        res = db.lintio.update({ "_id": ObjectId(record["_id"])}, { "$set": \
        {"lint."+info["branch"]+"."+str(info['last_commit']): \
        elt }}, True, multi=True)
    except Exception as e:
        logging.error("# Exception: "+str(e))


    conn.close()


