import unittest
import transaction
import os

from pyramid import testing

from lintio.models import DBSession

from webob.multidict import MultiDict

class TestAnalyse(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('sqlite://')
        from .models import (
            Base,
            )
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_analyse(self):
        from lintio.views import analyse
        mdict = MultiDict()
        mdict.add('project','lintio')
        mdict.add('owner','osallou')
        mdict.add('url',' https://osallou@bitbucket.org/osallou/lint-io.git')
        mdict.add('commit','408dd072633a54b5ce545b1cd19976664f6663b5')
        mdict.add('branch','master')
        curdir = os.path.dirname(os.path.realpath(__file__))
        lint_example = os.path.join(curdir,'..','..','examples','.lint-io')
        request = testing.DummyRequest(mdict)
        request.registry = self.config.registry
        request.registry.settings['lint'] = lint_example
        request.registry.settings['mongo_server'] = 'localhost'
        request.registry.settings['mongo_port'] = 27017
        request.registry.settings['delay'] = 0
        request.registry.settings['repo'] = '/tmp'
        info = analyse(request)
        #self.assertEqual(info['project'], 'lint-io')

